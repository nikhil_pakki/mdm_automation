########################################################
# Read predicted file and rule file                    #
# apply SU, IU, PEq rules from rules file for each row #
########################################################

import sys
import traceback
import pandas as pd
import ast
import logging


def main(sheet_name):
    logger_code = logging.getLogger("mdm_automation_code_logger_namespace")

    df_model = pd.read_csv('mdm_automation_package/intermediate_data_files/I4_after_classification_on_category.csv')
    df_model['NFC_model'].fillna(df_model['NFC123_Recode'], inplace=True)
    # print(df_model.shape)

    df_logic_su = pd.read_excel('../../data/mdm_automation/rules/test_run_rules.xlsx', sheet_name='SU')
    ### df_logic_su['Dividend'] = df_logic_su['Dividend'].fillna('1')
    ### df_logic_su['Divisor'] = df_logic_su['Divisor'].fillna('1')

    df_logic_peq = pd.read_excel('../../data/mdm_automation/rules/test_run_rules.xlsx', sheet_name='Peq')
    ### df_logic_peq['PEq'] = df_logic_peq['PEq'].fillna('1')
    #
    df_logic_iu = pd.read_excel('../../data/mdm_automation/rules/test_run_rules.xlsx', sheet_name='IU')

    logger_code.info("Applying rules for SU, IU and PEq")
    for df_model_row_index, data_row in df_model.iterrows():  # iterate through each row of data
        # if df_model_row_index >314:
        #     print()
        ################
        # # SU logic # #
        ################
        try:
            # #  extract from column SU, G and ML, default 1
            # # SU_model
            if pd.isna(data_row['SU_model']):
                su_v = 1
            elif 'x' in str(data_row['SU_model']).lower() or ' ' in str(data_row['SU_model']).lower():
                su_array = str(data_row['SU_model']).lower().strip().replace(' ', 'x').split('x')
                su_v = 1
                for su_array_ele in su_array:
                    if len(''.join(char for char in str(su_array_ele) if char.isdigit() or char in '.')) == 0:
                        su_array_ele = 1
                    else:
                        su_array_ele = ''.join(char for char in str(su_array_ele) if char.isdigit() or char in '.')
                        if su_array_ele == '.':
                            su_array_ele = 1
                        else:
                            su_array_ele = float(su_array_ele)
                    su_v = su_v * su_array_ele
            else:
                su_v = float(''.join(char for char in str(data_row['SU_model']) if char.isdigit() or char in '.'))

            # # SU_G_model
            if pd.isna(data_row['SU_G_model']):
                g_v = 1
            elif 'x' in str(data_row['SU_G_model']).lower() or ' ' in str(data_row['SU_G_model']):
                su_g_array = str(data_row['SU_G_model']).lower().strip().replace(' ', 'x').split('x')
                g_v = 1
                for su_g_array_ele in su_g_array:
                    if len(''.join(char for char in str(su_g_array_ele) if char.isdigit() or char in '.')) == 0:
                        su_g_array_ele = 1
                    else:
                        su_g_array_ele = ''.join(char for char in str(su_g_array_ele) if char.isdigit() or char in '.')
                        if su_g_array_ele == '.':
                            su_g_array_ele = 1
                        else:
                            su_g_array_ele = float(su_g_array_ele)
                    g_v = g_v * su_g_array_ele
            else:
                g_v = float(''.join(char for char in str(data_row['SU_G_model']) if char.isdigit() or char in '.'))

            # # ML_primary_model
            if pd.isna(data_row['ML_primary_model']):
                ml_v = 1
            elif 'x' in str(data_row['ML_primary_model']).lower() or ' ' in str(data_row['ML_primary_model']).lower():
                ml_array = str(data_row['ML_primary_model']).lower().strip().replace(' ', 'x').split('x')
                ml_v = 1
                for ml_array_ele in ml_array:
                    if len(''.join(char for char in str(ml_array_ele) if char.isdigit() or char in '.')) == 0:
                        ml_array_ele = 1
                    else:
                        ml_array_ele = ''.join(char for char in str(ml_array_ele) if char.isdigit() or char in '.')
                        if ml_array_ele == '.':
                            ml_array_ele = 1
                        else:
                            ml_array_ele = float(ml_array_ele)
                    ml_v = ml_v * ml_array_ele
            else:
                ml_v = float(''.join(char for char in str(data_row['ML_primary_model']) if char.isdigit() or char in '.'))

            # g_v = su_v*g_v

            for _, rules_row in df_logic_su.iterrows():  # iterate through each row of rule
                rule_found = True
                for row_index in range(7):  # iterate through each cell of rules row
                    column_name = rules_row.index[row_index]
                    column_value = rules_row[row_index]
                    if pd.isna(column_value):
                        continue

                    data_val = data_row[column_name]
                    data_val = str(data_val).lower().strip()
                    # data_val = [data_val.lower().strip() if isinstance(data_val, str) else data_val]
                    if '[' in str(column_value):   # is a list-> other operations
                        column_value = ast.literal_eval(column_value)
                        operation = column_value[0]
                        value = column_value[1]
                        value = [str(v).lower().strip() for v in value]
                        # for i in range(len(value)):
                        #     if isinstance(value[i], str):
                        #         value[i] = value[i].lower()
                        #     else:
                        #         value[i] = str(value[i])
                        # value = [str(v).lower() for v in value]
                        if operation == '!=':
                            if data_val in value:
                                rule_found = False
                                break
                        elif operation == '=':
                            if column_name == 'ML_primary_model':
                                if ml_v != float(value[0]):
                                    rule_found = False
                                    break
                            elif column_name == 'SU_G_model':
                                if g_v != float(value[0]):
                                    rule_found = False
                                    break
                            elif data_val not in value:
                                rule_found = False
                                break
                        elif operation == 'contains':
                            if any(data_val in string for string in value):
                                rule_found = False
                                break
                        elif operation == '<':
                            if column_name == 'ML_primary_model':
                                if ml_v >= float(value[0]):
                                    rule_found = False
                                    break
                            elif column_name == 'SU_G_model':
                                if g_v >= float(value[0]):
                                    rule_found = False
                                    break
                            elif float(data_val) >= float(value[0]):
                                rule_found = False
                                break
                        elif operation == '>':
                            if column_name == 'ML_primary_model':
                                if ml_v <= float(value[0]):
                                    rule_found = False
                                    break
                            elif column_name == 'SU_G_model':
                                if g_v <= float(value[0]):
                                    rule_found = False
                                    break
                            elif float(data_val) <= float(value[0]):
                                rule_found = False
                                break
                    elif data_val != column_value.lower().strip():
                        rule_found = False
                        break

                if rule_found:
                    dividend = str(rules_row['Dividend'])
                    divisor = str(rules_row['Divisor'])

                    # check if there are multiple rules for same nfc, if this is the first one and has blank then skip
                    if pd.notna(rules_row['Multiple rules']) and str(rules_row['Multiple rules']).lower().strip() == 'yes':
                        # check if key column is null
                        if dividend == 'SU' or dividend == 'SU[0]':
                            if pd.isna(data_row['SU_model']):
                                continue  ## skip this rule and wait for the next one
                        elif dividend == 'G':
                            if pd.isna(data_row['SU_G_model']):
                                continue  ## skip this rule and wait for the next one
                        elif dividend == 'ML':
                            if pd.isna(data_row['ML_primary_model']):
                                continue  ## skip this rule and wait for the next one
                    g_v = su_v * g_v
                    if dividend == 'SU':
                        dividend = su_v
                    elif dividend == 'SU[0]':
                        dividend = ('1' if pd.isna(data_row['SU_model']) else str(data_row['SU_model'])).lower().strip().replace(' ', '').split('x')[0]
                        dividend = float(''.join(char for char in dividend if char.isdigit() or char in '.'))
                    elif dividend == 'G':
                        dividend = g_v
                    elif dividend == 'ML':
                        dividend = ml_v
                    else:
                        dividend = float(dividend)

                    if divisor == 'SU':
                        divisor = su_v
                    elif divisor == 'G':
                        divisor = g_v
                    elif divisor == 'ML':
                        divisor = ml_v
                    else:
                        divisor = float(divisor)

                    df_model.loc[df_model_row_index, 'SU_C'] = dividend / divisor
                    break
        except Exception as e:
            # traceback.print_exc(file=sys.stdout)
            # print(df_model_row_index + 1, 'Error in SU:', sys.exc_info()[2].tb_lineno, ' : ', e)
            logger_code.error(
                "******************************* Error Occurred @ C5 !! ********************************")
            logger_code.error(e)
            logger_code.error(traceback.print_exc(file=sys.stdout))
            df_model.loc[df_model_row_index, 'SU_C'] = 'Error' + str(sys.exc_info()[2].tb_lineno) + " : " + str(e)

        ################
        # # PEq logic # #
        ################
        try:
            for _, rules_row in df_logic_peq.iterrows():
                rule_found = True
                for row_index in range(8):  # iterate through each cell of rules row
                    column_name = rules_row.index[row_index]
                    column_value = rules_row[row_index]

                    if pd.isna(column_value):
                        continue

                    column_value = str(column_value).lower().strip()
                    data_val = str(data_row[column_name]).lower().strip()

                    if column_name == 'MG_primary_model':
                        data_val = float(''.join(char for char in data_val if char.isdigit() or char in '.'))
                        column_value = float(column_value)

                    if '[' in column_value:
                        column_value = ast.literal_eval(column_value)
                        operation = str(column_value[0]).strip()
                        value = column_value[1]
                        value = [str(v).lower().strip() for v in value]
                        if operation == '!=':
                            if data_val in value:
                                rule_found = False
                                break
                        elif operation == '=':
                            if data_val not in value:
                                rule_found = False
                                break
                        # elif operation == 'contains':
                        #     if data_row[column_name].lower().strip() in value:
                        #         rule_found = False
                        #         break

                    elif data_val != column_value:
                        rule_found = False
                        break
                if rule_found:
                    df_model.loc[df_model_row_index, 'PEq_C'] = float(rules_row['PEq'])
                    break
        except Exception as e:
            logger_code.error(
                "******************************* Error Occurred @ C5 !! ********************************")
            logger_code.error(e)
            logger_code.error(traceback.print_exc(file=sys.stdout))
            # print(df_model_row_index + 1, 'Error in PEq:', e)
            df_model.loc[df_model_row_index, 'PEq_C'] = 'Error' + str(sys.exc_info()[2].tb_lineno) + " : " + str(e)

        # ################
        # # # IU logic # #
        # ################
        try:
            iu_raw = '100' if pd.isna(data_row['IU_model']) else str(data_row['IU_model']).lower().replace(' ', '')
            # # iu_val calculation
            if '/' in iu_raw:
                iu_arr = iu_raw.split('/')
                if len(iu_arr) > 2:
                    raise Exception('IU value incorrectly predicted')
                else:
                    iu_arr_1 = float(''.join(char for char in iu_arr[0] if char.isdigit() or char in '.'))
                    iu_arr_2 = (''.join(char for char in iu_arr[1] if char.isdigit() or char in '.'))
                    if pd.isna(iu_arr_2) or iu_arr_2 == 0 or iu_arr_2 == '':
                        iu_arr_2 = 1
                    else:
                        iu_arr_2 = float(iu_arr_2)
                    iu_val = iu_arr_1/iu_arr_2
            else:
                iu_val = float(''.join(char for char in iu_raw if char.isdigit() or char in '.'))

            # # ML_primary_model
            if pd.isna(data_row['ML_primary_model']):
                ml_v = 3
            else:
                ml_v = float(''.join(char for char in data_row['ML_primary_model'] if char.isdigit() or char in '.'))

            for _, rules_row in df_logic_iu.iterrows():  # iterate through each row of rule
                rule_found = True
                for row_index in range(7):  # iterate through each cell of rules row
                    column_name = rules_row.index[row_index]
                    column_value = rules_row[row_index]
                    if pd.isna(column_value):
                        continue
                    data_val = 'column is blank' if pd.isna(data_row[column_name]) else data_row[column_name].lower().replace(' ', '')
                    if '[' in column_value:
                        column_value = ast.literal_eval(column_value)
                        operation = column_value[0]
                        value = column_value[1]
                        value = [str(v).lower().replace(' ', '') for v in value]
                        if operation == '!=':
                            if data_val in value:
                                rule_found = False
                                break
                        elif operation == '=':
                            if column_name == 'IU_model':
                                if iu_val != float(value[0]):
                                    rule_found = False
                                    break
                            elif data_val not in value:
                                rule_found = False
                                break
                        elif operation == '<':
                            if column_name == 'IU_model':
                                if iu_val >= float(value[0]):
                                    rule_found = False
                                    break
                        elif operation == '>':
                            if column_name == 'IU_model':
                                if iu_val <= float(value[0]):
                                    rule_found = False
                                    break
                        elif operation == 'contains':
                            if column_name == 'IU_model':
                                if value[0] not in iu_raw:
                                    rule_found = False
                                    break
                            elif not any(x in data_val for x in value):
                                # if data_row[column_name].lower().strip().contains(value):
                                rule_found = False
                                break

                    elif data_val != column_value.lower().replace(' ',''):
                        rule_found = False
                        break
                if rule_found:
                    dividend = str(rules_row['Dividend'])
                    divisor = str(rules_row['Divisor'])

                    # dividend
                    if dividend == 'IU':
                        dividend = iu_val
                    # elif dividend == 'MG':
                    #     if pd.isna(data_row['Concentration model']):
                    #         data_row['Concentration model'] = '-1'
                    #     dividend = data_row['Concentration model'].lower().replace('mg', '').replace('ml', '').strip()
                    #     # dividend = int(dividend)
                    dividend = float(dividend)

                    # divisor
                    if divisor == 'IU':
                        divisor = iu_val
                    elif divisor == 'ML':
                        divisor = ml_v
                    elif divisor == '1/ML':
                        divisor = 1/ml_v
                    elif divisor == 'ML/3':
                        divisor = 3/ml_v

                    divisor = float(divisor)

                    df_model.loc[df_model_row_index, 'IU_C'] = dividend / divisor
                    break
        except Exception as e:
            # traceback.print_exc(file=sys.stdout)
            # print(df_model_row_index + 1, 'Error in IU', sys.exc_info()[2].tb_lineno, ' : ', e)
            logger_code.error(
                "******************************* Error Occurred @ C5 !! ********************************")
            logger_code.error(e)
            logger_code.error(traceback.print_exc(file=sys.stdout))
            df_model.loc[df_model_row_index, 'IU_C'] = 'Error @ ' + str(sys.exc_info()[2].tb_lineno) + " : " + str(e)

    df_empty = pd.read_csv('mdm_automation_package/intermediate_data_files/I1_empty_pack_recode.csv')
    df_model = df_model.append(df_empty, ignore_index=True)
    df_model.to_excel("../../data/mdm_automation/I5_after_rules_implementation_"+sheet_name+".xlsx", index=False)


if __name__ == '__main__':
    main(sheet_name='Sheet1')
