######################################################################################
# Read predicted result                                                              #
# categories pack_recode into different columns based on category that was predicted #
######################################################################################

import pandas as pd
import ast
import logging


def main():
    logger_code = logging.getLogger("mdm_automation_code_logger_namespace")


    df = pd.read_csv('mdm_automation_package/intermediate_data_files/I3_after_pack_recode_prediction.csv')

    # char_dict = {
    #     'a': 0,
    #     'b': 1,
    #     'c': 2,
    #     'd': 3,
    #     'e': 4,
    #     'f': 5,
    #     'g': 6,
    #     'h': 7,
    #     'i': 8,
    #     'j': 9,
    #     'k': 10,
    #     'l': 11,
    #     'm': 12,
    #     'n': 13,
    #     'o': 14,
    #     'p': 15,
    #     'q': 16,
    #     'r': 17,
    #     's': 18,
    #     't': 19,
    #     'u': 20,
    #     'v': 21,
    #     'w': 22,
    #     'x': 23,
    #     'y': 24,
    #     'z': 25,
    #     '0': 26,
    #     '1': 27,
    #     '2': 28,
    #     '3': 29,
    #     '4': 30,
    #     '5': 31,
    #     '6': 32,
    #     '7': 33,
    #     '8': 34,
    #     '9': 35,
    #     '-': 36,
    #     '/': 37,
    #     '.': 38,
    #     '#': 39,
    #     '(': 40,
    #     ')': 41,
    #     '%': 42,
    #     ' ': 43,
    #     ',': 44,
    #     '+': 45,
    #     '&': 46,
    #     ';': 47
    # }

    # df[['Dosage_type_model', 'ML_primary_model', 'ML_other_model', 'MG_primary_model', 'MG_other_model', 'IU_model',
    #     'SU_model', 'SU_G_model', 'Others_model']] = ''
    df = df.assign(Dosage_type_model='', ML_primary_model='', ML_other_model='', MG_primary_model='', MG_other_model='', IU_model='', SU_model='', SU_G_model='', Others_model='')
    for row_index, row in df.iterrows():
        df_pack_recode_cat = pd.DataFrame(data={'value': [[], [], [], [], [], [], [], [], []]})
        prev_cat = 1
        pack_recode = str(row['Pack_recode'])
        category = ast.literal_eval(row['Category_model'].replace(' ', ','))
        index_category = 0
        for index_char, character in enumerate(pack_recode):
            #         if character.lower() not in char_dict.keys():
            #             df_pack_recode_cat.iloc[prev_cat-1, 0].append(character)
            #             continue

            if prev_cat != category[index_category]:
                df_pack_recode_cat.iloc[prev_cat - 1, 0].append(' ')

            if category[index_category] == 0:
                if prev_cat != 9:
                    df_pack_recode_cat.iloc[prev_cat - 1, 0].append(character)
                elif len(category) > index_category + 1 and category[index_category + 1] != 0:
                    df_pack_recode_cat.iloc[category[index_category + 1] - 1, 0].append(character)
                else:
                    df_pack_recode_cat.iloc[9 - 1, 0].append(character)
            else:
                prev_cat = category[index_category]
                df_pack_recode_cat.iloc[category[index_category] - 1, 0].append(character)
            index_category += 1

        for index in range(9):
            df_pack_recode_cat.loc[index, 'value'] = (''.join(df_pack_recode_cat.loc[index, 'value'])).strip()

        df.loc[row_index, 'Dosage_type_model'] = df_pack_recode_cat.loc[0, 'value']
        df.loc[row_index, 'ML_primary_model'] = df_pack_recode_cat.loc[1, 'value']
        df.loc[row_index, 'ML_other_model'] = df_pack_recode_cat.loc[2, 'value']
        df.loc[row_index, 'MG_primary_model'] = df_pack_recode_cat.loc[3, 'value']
        df.loc[row_index, 'MG_other_model'] = df_pack_recode_cat.loc[4, 'value']
        df.loc[row_index, 'IU_model'] = df_pack_recode_cat.loc[5, 'value']
        df.loc[row_index, 'SU_model'] = df_pack_recode_cat.loc[6, 'value']
        df.loc[row_index, 'SU_G_model'] = df_pack_recode_cat.loc[7, 'value']
        df.loc[row_index, 'Others_model'] = df_pack_recode_cat.loc[8, 'value']

        if row_index % 1_000 == 0:
            logger_code.info("checkpoint: number of rows processed- "+ str(row_index))

    df.to_csv('mdm_automation_package/intermediate_data_files/I4_after_classification_on_category.csv', index=False)


if __name__ == '__main__':
    main()
