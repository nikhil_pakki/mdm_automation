#######################################################################################################
# Vectorize pack_recode                                                                               #
# Predict NFC                                                                                         #
# Merge 3 output columns into 1 and calculate confidence based on round(10000/(1+1.5*(index_char**4)) #
#######################################################################################################

import traceback, sys
import pandas as pd
# import re
import numpy as np
# import ast
import os
import logging
from tensorflow import keras
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'


def main():
    logger_code = logging.getLogger("mdm_automation_code_logger_namespace")
    logger_code.info("Loading NFC prediction model..")
    # read NFC model
    model = keras.models.load_model('mdm_automation_package/model/NFC_model')

    logger_code.info("Reading input file..")
    # read input file
    df = pd.read_csv('mdm_automation_package/intermediate_data_files/I1_after_product_segment_mapping.csv')

    char_dict = {
        'a': 0,
        'b': 1,
        'c': 2,
        'd': 3,
        'e': 4,
        'f': 5,
        'g': 6,
        'h': 7,
        'i': 8,
        'j': 9,
        'k': 10,
        'l': 11,
        'm': 12,
        'n': 13,
        'o': 14,
        'p': 15,
        'q': 16,
        'r': 17,
        's': 18,
        't': 19,
        'u': 20,
        'v': 21,
        'w': 22,
        'x': 23,
        'y': 24,
        'z': 25,
        '0': 26,
        '1': 27,
        '2': 28,
        '3': 29,
        '4': 30,
        '5': 31,
        '6': 32,
        '7': 33,
        '8': 34,
        '9': 35,
        '-': 36,
        '/': 37,
        '.': 38,
        '#': 39,
        '(': 40,
        ')': 41,
        '%': 42,
        ' ': 43,
        ',': 44,
        '+': 45,
        '&': 46,
        ';': 47

    }
    not_in_char_dict = []

    df_c = df.copy()

    # if len(df_c[pd.isna(df_c['Pack_recode'])]) != 0:
    #     print('empty pack recode rows found. count:', len(df_c[pd.isna(df_c['Pack_recode'])]))
    # remove empty pack recode
    df_c = df_c[pd.notna(df_c['Pack_recode'])].copy()

    df_c['Pack_recode'] = df_c['Pack_recode'].astype(str)

    # get all NFC==UNKNOWN and blank
    df_unknown_nfc = df_c[df_c['NFC123_Recode'].str.strip().str.upper() == 'UNKNOWN'].reset_index(drop=True).copy()

    if not df_unknown_nfc.empty:
        df_c['NFC_model'] = ''
        df_c['Confidence_NFC_mode'] = ''

        x = np.zeros((len(df_unknown_nfc), 111, 49), dtype=np.bool)
        # x vectorization
        for index_row, row in df_unknown_nfc.iterrows():
            try:
                # # row by row
                for index_char, char in enumerate(str(row['Pack_recode']).lower()):
                    if char in char_dict:
                        x[index_row, index_char, char_dict[char]] = 1
                    else:
                        x[index_row, index_char, 48] = 1
                        if char not in not_in_char_dict:
                            not_in_char_dict.append(char)
                            logger_code.warning("CHARACTER: '", str(char), "' IS A NEW INPUT AND IS NOT PROCESSED AS "
                                                                           "A INDEPENDENT CHARACTER")
            except Exception as e:
                # print('index_row: ', index_row)
                # print('Pack_recode_model: ', row['Pack_recode'])
                # print(e)
                # traceback.print_exc(file=sys.stdout)
                logger_code.error(
                    "******************************* Error Occurred @ C2 !! ********************************")
                logger_code.error(e)
                logger_code.error(traceback.print_exc(file=sys.stdout))

        logger_code.info("Predicting NFC_123...")
        # # PREDICT
        result = model.predict(x)

        # change bool prediction output to NFC_123 format
        y_check = []
        confidence_arr = []
        for index_row, test_row in enumerate(result):  # for each row
            try:
                test_row_array = [0] * 3
                confidence = 0
                count = 0
                for index_char, test_char in enumerate(test_row[:3]):
                    confidence += [max(test_char) if max(test_char).round()!=0 else 0][0] * round(10000/(1+1.5*(index_char**4)))
                    count += round(10000/(1+1.5*(index_char**4)))
                    test_char = test_char.round()
                    if len(np.where(test_char == 1)[0]) == 0:
                        test_row_array[index_char] = '0'
                    else:
                        test_row_array[index_char] = str(list(char_dict.keys())[list(char_dict.values()).index(int(np.where(test_char == 1)[0][0]))])# int(np.where(test_char == 1)[0][0])

                df_c.at[df_c[df_c['ID_Transcode'] == df_unknown_nfc.at[index_row, 'ID_Transcode']].index[0], 'NFC_model'] = ''.join(test_row_array).upper()
                df_c.at[df_c[df_c['ID_Transcode'] == df_unknown_nfc.at[index_row, 'ID_Transcode']].index[0], 'Confidence_NFC_mode'] = round(confidence/count, 3)
            except Exception as e:
                # print('index_row: ', index_row)
                # print('Pack_recode_model: ', row['Pack_recode_model'])
                # print(e)
                # traceback.print_exc(file=sys.stdout)
                logger_code.error(
                    "******************************* Error Occurred @ C2 !! ********************************")
                logger_code.error(e)
                logger_code.error(traceback.print_exc(file=sys.stdout))
    else:
        df_c['NFC_model'] = df_c['NFC123_Recode']
        df_c['Confidence_NFC_mode'] = 1

    df_c.to_csv('mdm_automation_package/intermediate_data_files/I2_after_nfc_prediction.csv', index=False)


if __name__ == '__main__':
    main()

