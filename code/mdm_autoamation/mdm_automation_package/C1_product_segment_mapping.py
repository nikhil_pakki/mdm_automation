############################################################
# # Read raw file                                          #
# # save rows with pack_recode=null in separate file       #
# # do product mapping based on rules given in file        #
# # do segment mapping based on rules given in file        #
############################################################

import logging
import sys
import traceback
import pandas as pd
import numpy as np


def main(sheet_name):
    logger_code = logging.getLogger("mdm_automation_code_logger_namespace")
    file_name = 'transcode_file.xlsx'  # 'Classical_10th_Nov_Compiled.xlsx' #'iu_test.xlsx'
    # sheet_name = sheet_name  # 'Total Genmed rows'
    df_input = pd.read_excel('../../data/mdm_automation/' + file_name, sheet_name=sheet_name)

    if sheet_name == 'GenMed':
        df_input['Category_model_2'] = df_input['Category'].str.rsplit('_', 1, expand=True)[0]
        df_input['Category_model_2'] = df_input['Category_model_2'].str.strip()
    else:  # classical
        df_input['Category_model_2'] = df_input['Category']

    logger_code.info('Number of empty Pack_recode:' + str(len(df_input[pd.isna(df_input['Pack_recode'])])))

    # if len(df_input[pd.isna(df_input['Pack_recode'])]) != 0:
    #     pass

    df_empty = df_input[pd.isna(df_input['Pack_recode'])].copy()
    df_empty.to_csv('mdm_automation_package/intermediate_data_files/I1_empty_pack_recode.csv', index=False)
    # remove empty pack recode
    df_input = df_input[pd.notna(df_input['Pack_recode'])].copy()

    logger_code.info("Reading rules file...")
    df_pg_rules = pd.read_excel('../../data/mdm_automation/rules/test_run_rules.xlsx', sheet_name='Product grouping')
    df_segment_rules = pd.read_excel('../../data/mdm_automation/rules/test_run_rules.xlsx', sheet_name='Segment column')

    column_list_pg = df_pg_rules.columns
    df_input['Product_Group_1_model'] = np.NaN
    df_input['Product_Group_2_model'] = np.NaN
    df_input['Product_Group_3_model'] = np.NaN
    df_input['Segment_model'] = np.NaN

    logger_code.info("Applying product grouping and segment rules to rows...")
    for index_df_input, rows_df_input in df_input.iterrows():
        try:
            for index_rules, row_rules in df_pg_rules.iterrows():
                if pd.notna(rows_df_input['Product_Group_1_C']):
                    continue
                rule_found = True
                # if index_rules > 250:
                #     print('index_rules > 160')
                for i in range(0, df_pg_rules.shape[1] - 3, 2):
                    # print(i)
                    # print(column_list[i], column_list[i + 1])
                    if pd.isna(row_rules[i + 1]):
                        continue
                    elif pd.isna(rows_df_input[column_list_pg[i + 1]]):
                        rule_found = False
                        break
                    else:
                        rules_value = row_rules[i + 1].split(';')
                        rules_value = [rules_value_ele.strip().upper() for rules_value_ele in rules_value if
                                       rules_value_ele != '']
                        column_name = column_list_pg[i + 1]
                        if row_rules[i] == '=':
                            # equal
                            if rows_df_input[column_name].upper() not in rules_value:
                                rule_found = False
                                break
                        elif row_rules[i] == '<>':
                            # Not equal
                            if rows_df_input[column_name].upper() in rules_value:
                                rule_found = False
                                break
                        elif row_rules[i].lower().strip() == 'like':
                            # contains
                            if not any(string in rows_df_input[column_name].upper() for string in rules_value):
                                rule_found = False
                                break
                        elif row_rules[i].lower().strip() == 'not like':
                            # not contains
                            if not all(string not in rows_df_input[column_name].upper() for string in rules_value):
                                rule_found = False
                                break
                if rule_found:
                    df_input.loc[index_df_input, 'Product_Group_1_model'] = row_rules['Product_Group_1']
                    df_input.loc[index_df_input, 'Product_Group_2_model'] = row_rules['Product_Group_2']
                    df_input.loc[index_df_input, 'Product_Group_3_model'] = row_rules['Product_Group_3']
                    break
        except Exception as e:
            logger_code.error("******************************* Error Occurred @ C1 !! ********************************")
            logger_code.error(e)
            logger_code.error(traceback.print_exc(file=sys.stdout))
            # print('index_df_input', index_df_input)
            # # print('index_rules', index_rules)
            # # print('i', i)
            # print(e)
            # exit()

        try:
            column_list_seg = df_segment_rules.columns
            for index_rules, row_rules in df_segment_rules.iterrows():
                rule_found = True
                for i in range(0, df_segment_rules.shape[1] - 1, 2):
                    if pd.isna(row_rules[i + 1]):
                        continue
                    elif pd.isna(rows_df_input[column_list_seg[i + 1]]):
                        rule_found = False
                        break
                    else:
                        rules_value = row_rules[i + 1].split(';')
                        rules_value = [rules_value_ele.strip().upper() for rules_value_ele in rules_value if
                                       rules_value_ele != '']

                        column_name = column_list_seg[i + 1]
                        if row_rules[i] == '=':
                            # equal
                            if rows_df_input[column_name].upper() not in rules_value:
                                rule_found = False
                                break
                        elif row_rules[i] == '<>':
                            # Not equal
                            if rows_df_input[column_name].upper() in rules_value:
                                rule_found = False
                                break
                        elif row_rules[i].lower().strip() == 'like':
                            # contains
                            if not any(string in rows_df_input[column_name].upper() for string in rules_value):
                                rule_found = False
                                break
                        elif row_rules[i].lower().strip() == 'not like':
                            # not contains
                            if not all(string not in rows_df_input[column_name].upper() for string in rules_value):
                                rule_found = False
                                break
                if rule_found:
                    df_input.loc[index_df_input, 'Segment_model'] = row_rules['Segment']
                    break
        except Exception as e:
            logger_code.error("******************************* Error Occurred @ C1 !! ********************************")
            logger_code.error(e)
            logger_code.error(traceback.print_exc(file=sys.stdout))
            # print('index_df_input', index_df_input)
            # # print('index_rules', index_rules)
            # # print('i', i)
            # print(e)
            # traceback.print_exc(file=sys.stdout)
    df_input['Product_Group_1_model'].fillna(df_input['Product_Group_1_C'], inplace=True)
    df_input['Product_Group_2_model'].fillna(df_input['Product_Group_2_C'], inplace=True)
    df_input['Product_Group_3_model'].fillna(df_input['Product_Group_3_C'], inplace=True)
    df_input.to_csv('mdm_automation_package/intermediate_data_files/I1_after_product_segment_mapping.csv', index=False)


# this code should never be called directly. Only way to call is from run.py -> call.py -> C1_product_segment_mapping.py
if __name__ == '__main__':
    main(sheet_name='Sheet1')
