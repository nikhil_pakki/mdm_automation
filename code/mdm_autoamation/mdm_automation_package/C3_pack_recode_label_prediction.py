################################################################################
# Vectorize pack_recode                                                        #
# Predict category of pack_recode                                              #
# reduce 3d result into 2d(assign category instead of vector for each category #
################################################################################

import sys
import traceback
import pandas as pd
import numpy as np
from tensorflow import keras
import os
import logging
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'


def main():
    logger_code = logging.getLogger("mdm_automation_code_logger_namespace")
    
    logger_code.info("Loading Pack_recode labeling model...")
    model = keras.models.load_model('mdm_automation_package/model/pack_recode_label_model')

    char_dict = {
        'a': 0,
        'b': 1,
        'c': 2,
        'd': 3,
        'e': 4,
        'f': 5,
        'g': 6,
        'h': 7,
        'i': 8,
        'j': 9,
        'k': 10,
        'l': 11,
        'm': 12,
        'n': 13,
        'o': 14,
        'p': 15,
        'q': 16,
        'r': 17,
        's': 18,
        't': 19,
        'u': 20,
        'v': 21,
        'w': 22,
        'x': 23,
        'y': 24,
        'z': 25,
        '0': 26,
        '1': 27,
        '2': 28,
        '3': 29,
        '4': 30,
        '5': 31,
        '6': 32,
        '7': 33,
        '8': 34,
        '9': 35,
        '-': 36,
        '/': 37,
        '.': 38,
        '#': 39,
        '(': 40,
        ')': 41,
        '%': 42,
        ' ': 43,
        ',': 44,
        '+': 45,
        '&': 46,
        ';': 47
    }
    not_in_char_dict = []

    df = pd.read_csv('mdm_automation_package/intermediate_data_files/I2_after_nfc_prediction.csv')

    df_c = df.copy()
    df_c['Pack_recode'] = df_c['Pack_recode'].astype(str)

    x_check = np.zeros((len(df_c), 111, 49), dtype=np.bool)

    for index_row, row in df_c.iterrows():
        try:
            # for x vectorization
            for index_char, char in enumerate(str(row['Pack_recode']).lower()):
                if char in char_dict:
                    x_check[index_row, index_char, char_dict[char]] = 1
                else:
                    x_check[index_row, index_char, 48] = 1
                    if char not in not_in_char_dict:
                        not_in_char_dict.append(char)
                        # print('**--> ', char, ' is not present in the dictionary')
        except Exception as e:
            # print('index_row: ', index_row)
            # print('Pack_recode_model: ', row['Pack_recode'])
            # traceback.print_exc(file=sys.stdout)
            logger_code.error(
                "******************************* Error Occurred @ C3 !! ********************************")
            logger_code.error(e)
            logger_code.error(traceback.print_exc(file=sys.stdout))

    logger_code.info("Predicting lables for Pack_recode...")
    # # PREDICT
    result = model.predict(x_check)

    y_check = []
    confidence_arr = []
    for index_row, test_row in enumerate(result):  # for each row
        try:
            test_row_array = np.zeros(111, dtype=int)
            row_max_length = len(df_c.loc[index_row, 'Pack_recode'])
            confidence = 0
            count = 0
            for index_char, test_char in enumerate(test_row[:row_max_length]):
                confidence += [max(test_char) if max(test_char).round() != 0 else 0][0]
                count += 1
                test_char = test_char.round()
                if len(np.where(test_char == 1)[0]) == 0:
                    test_row_array[index_char] = 0
                else:
                    test_row_array[index_char] = int(np.where(test_char == 1)[0][0])
        #             test_row_array.append()
        #     print(index_row, confidence, count, round(confidence/count,3))
            confidence_arr.append(round(confidence/count,3))
            y_check.append(test_row_array)
        except Exception as e:
            # print('index_row: ', index_row)
            # print('Pack_recode_model: ', row['Pack_recode'])
            # traceback.print_exc(file=sys.stdout)
            logger_code.error(
                "******************************* Error Occurred @ C3 !! ********************************")
            logger_code.error(e)
            logger_code.error(traceback.print_exc(file=sys.stdout))

    df_c['Category_model'] = pd.Series(y_check)
    df_c['Confidence_pack_recode_mode'] = pd.Series(confidence_arr)

    df_c.to_csv('mdm_automation_package/intermediate_data_files/I3_after_pack_recode_prediction.csv', index=False)


if __name__ == '__main__':
    main()
