from mdm_automation_package import C1_product_segment_mapping as c1
from mdm_automation_package import C2_nfc_prediction as c2
from mdm_automation_package import C3_pack_recode_label_prediction as c3
from mdm_automation_package import C4_category_classfication as c4
from mdm_automation_package import C5_rules_implementation as c5
import logging


def main(sheet_name):
    logger_code = logging.getLogger("mdm_automation_code_logger_namespace")

    logger_code.info('C1: Product grouping and segment mapping started...')
    c1.main(sheet_name)
    logger_code.info('C1: Product grouping and segment mapping complete!')

    logger_code.info('C2: NFC prediction started...')
    c2.main()
    logger_code.info('C2: NFC prediction complete!')

    logger_code.info('C3: Pack recode label prediction started...')
    c3.main()
    logger_code.info('C3: Pack recode label prediction complete!')

    logger_code.info('C4: Pack recode distribution based on prediction started...')
    c4.main()
    logger_code.info('C4: Pack recode distribution based on prediction complete!')

    logger_code.info('C5: Processing rules for SU, IU and pEq started...')
    c5.main(sheet_name)
    logger_code.info('C5: Processing rules for SU, IU and pEq complete!')

    # logger_code.info('Complete! Returning to file watcher.')
    return True

