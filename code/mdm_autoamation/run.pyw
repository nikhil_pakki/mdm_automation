import time
import boto3
import re
import datetime
from mdm_automation_package import call
import logging
import pandas as pd

log_file_name_suffix_g = ''
s3_client_g = ''


def run_mdm_automation():
    logger_code = logging.getLogger("mdm_automation_code_logger_namespace")
    raw_excel_data = pd.ExcelFile('../../data/mdm_automation/transcode_file.xlsx')
    global s3_client_g
    logger_code.info("Processing GenMed rows started...")
    if 'GenMed' in raw_excel_data.sheet_names:
        _ = call.main(sheet_name='GenMed')
        s3_client_g.upload_file('../../data/mdm_automation/I5_after_rules_implementation_GenMed.xlsx',
                              'sinergi-mdm', 'download/GenMed.xlsx')
        logger_code.info("Processing GenMed rows ended...")
    else:
        logger_code.error("No sheet name as 'GenMed' found in the uploaded file...")
        logger_code.info("Skipping 'GenMed' sheet due to above error")

    logger_code.info("Processing classical rows started...")
    if 'Classical' in raw_excel_data.sheet_names:
        _ = call.main(sheet_name='Classical')
        s3_client_g.upload_file('../../data/mdm_automation/I5_after_rules_implementation_Classical.xlsx',
                              'sinergi-mdm', 'download/Classical.xlsx')
        logger_code.info("Processing classical rows ended...")
    else:
        logger_code.error("No sheet name as 'Classical' found in the uploaded file...")
        logger_code.info("Skipping 'Classical' sheet due to above error")
    return


def check_new_file_present_in_s3():
    logger_run = logging.getLogger("mdm_automation_run_logger_namespace")
    logger_code = logging.getLogger("mdm_automation_code_logger_namespace")

    ## establish connection with s3
    logger_run.info("Establishing connection with S3...")
    s3_client = boto3.client('s3', aws_access_key_id='AKIA4UHVOPDMFWV7IY5H',
                             aws_secret_access_key='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')
    global s3_client_g
    s3_client_g = s3_client
    logger_run.info("Connection established!")
    transcode_file_last_modified = datetime.datetime.now(datetime.timezone.utc)

    while True:
        ## check if new file is present in s3
        logger_run.info("Checking for new transcode files...")
        for key in s3_client.list_objects(Bucket='sinergi-mdm')['Contents']:
            if re.search('upload/.*.xlsx', key['Key']):
                logger_run.info(key)
                if key['LastModified'] > transcode_file_last_modified:
                    logger_run.info("*** New transcode file found! ***")
                    transcode_file_last_modified = key['LastModified']
                    s3_client.download_file('sinergi-mdm', 'test_run_rules.xlsx',
                                            '../../data/mdm_automation/rules/test_run_rules.xlsx')
                    s3_client.download_file('sinergi-mdm', key['Key'], '../../data/mdm_automation/transcode_file.xlsx')
                    # delink handler from code logger and re attach a fresh one
                    for attached_handler in logger_code.handlers[:]:  # remove all old handlers
                        logger_code.removeHandler(attached_handler)
                    # log.addHandler(fileh)  # set the new handler
                    mdm_automation_logger_configuration_code()
                    run_mdm_automation()

                    # push output file to s3
                    # s3_client.upload_file('../../data/mdm_automation/I5_after_rules_implementation_GenMed.xlsx',
                    #                       'sinergi-mdm', 'download/GenMed.xlsx')
                    # s3_client.upload_file('../../data/mdm_automation/I5_after_rules_implementation_Classical rows.xlsx',
                    #                       'sinergi-mdm', 'download/Classical_rows.xlsx')

                    # push log file to s3
                    s3_client.upload_file("log/log_" + log_file_name_suffix_g + ".log",
                                          'sinergi-mdm', "download/log_" + log_file_name_suffix_g + ".log")
                    logger_run.info("*** Transcode file processing completed! ***")
        time.sleep(30)


def mdm_automation_logger_configuration_code():
    # Create a custom logger
    logger_code = logging.getLogger('mdm_automation_code_logger_namespace')

    logging.getLogger('boto3').setLevel(logging.ERROR)
    logging.getLogger('botocore').setLevel(logging.ERROR)
    logging.getLogger('s3transfer').setLevel(logging.ERROR)
    logging.getLogger('urllib3').setLevel(logging.ERROR)

    # logger.debug("Hello World")
    logger_code.setLevel(logging.DEBUG)
    # logger_code.setLevel(logging.INFO)

    log_file_name_suffix = datetime.datetime.now().strftime('%d%b%Y_%H_%M_%S')

    # logging.basicConfig(filename="log/log_"+log_file_name_suffix+".log",
    #                     filemode='w',
    #                     level=logging.DEBUG,
    #                     # level=logging.INFO,
    #                     format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', datefmt='%d-%b-%Y %H:%M:%S'
    #                     )

    f_handler = logging.FileHandler("log/log_" + log_file_name_suffix + ".log", mode='w')

    f_format = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s', datefmt='%d-%b-%Y %H:%M:%S')
    f_handler.setFormatter(f_format)

    logger_code.addHandler(f_handler)
    global log_file_name_suffix_g
    log_file_name_suffix_g = log_file_name_suffix

    # logger.debug('This is a debug message')
    # logger.info('This is an info message')
    # logger.warning('This is a warning message')
    # logger.error('This is an error message')
    # logger.critical('This is a critical message')


def mdm_automation_logger_configuration_run():
    # Create a custom logger
    logger_run = logging.getLogger('mdm_automation_run_logger_namespace')

    # logging.basicConfig(filename="log/run/run_log.log",
    #                     filemode='w',
    #                     level=logging.DEBUG,
    #                     # level=logging.INFO,
    #                     format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', datefmt='%d-%b-%Y %H:%M:%S')

    logging.getLogger('boto3').setLevel(logging.CRITICAL)
    logging.getLogger('botocore').setLevel(logging.CRITICAL)
    logging.getLogger('s3transfer').setLevel(logging.CRITICAL)
    logging.getLogger('urllib3').setLevel(logging.CRITICAL)

    # logger.debug("Hello World")
    logger_run.setLevel(logging.DEBUG)
    # logger_run.setLevel(logging.INFO)

    # log_file_name_suffix = datetime.datetime.now().strftime('%d%b%Y_%H_%M_%S')
    #
    f_handler = logging.FileHandler("log/run/run_log.log", mode='w')

    f_format = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s', datefmt='%d-%b-%Y %H:%M:%S')
    f_handler.setFormatter(f_format)

    logger_run.addHandler(f_handler)

    # logger.debug('This is a debug message')
    # logger.info('This is an info message')
    # logger.warning('This is a warning message')
    # logger.error('This is an error message')
    # logger.critical('This is a critical message')


if __name__ == '__main__':
    mdm_automation_logger_configuration_run()
    logger_run = logging.getLogger("mdm_automation_run_logger_namespace")
    while True:
        try:
            check_new_file_present_in_s3()
        except Exception as e:
            logger_run.error(e)
